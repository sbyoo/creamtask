function add_directoryPath(  )

	% Get the name of current directory
	cur_path = fileparts( which( mfilename ) );
	
	% Add the sub-folders to the path
	%	Note) If there is any sub-directory to add, put them below
	%   1. Directories for operation of the task.
    addpath( fullfile( cur_path, 'util' ) );
	addpath( genpath( [ cur_path, '/util'] ) ); 
	
    addpath( fullfile( cur_path, 'data' ) );
	addpath( genpath( [ cur_path, '/data'] ) ); 
    
    addpath( fullfile( cur_path, 'images' ) );
	addpath( genpath( [ cur_path, '/images'] ) ); 
    
    addpath( fullfile( cur_path, 'sounds' ) );
	addpath( genpath( [ cur_path, '/sounds'] ) ); 
    
end