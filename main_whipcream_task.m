function main_whipcream_task( )

    %% Current function is to instantiate Java script version into MATLAB.
    %   The main reason of conversion is the clinical population. 
    %   The strobes and eye tracker is easier to function with the MATLAB.
    %   Eventually consider for Psycho-py. 
    
    % Copyright: Seng Bum Michael Yoo (2019-12-19).
    
    % Task description: The animal appears and head towards the house.
    %   Then they go head to certain object depending on combination of animal and house. 
    %   The goal of the subject is to provide whip cream to animal as much as possible. 
    
    % Input Source: mouse of computer (function being used will totally be different than joysticks). 
    
    % What is being parametrized?
    %   1. Number of the animal, house, and object.
    %   2. The whip cream: the source of measuring confidence. 
    %   3. Randomization of the space-object.
    %   4. In which trial index, new object and animal introduced (aiming to study generalization). 

    % Things to be fixed.
    %   1. In some trials, the animal is not shown. 
    %   2. Need to incorporate the background (since the change of background indicate new knowledge). 
    %   3. Maximum whipcream should be upto number of options at the screen (asof 2020-02-20).
    %   4. Lower bound of the cream number should be set (default = 1)(asof 2020-02-20). 
    
    warning off; clear all;close all; clc; sca;
    
    %	Java Heap Clear before whole session starts
    %     javaaddpath( which('MatlabGarbageCollector.jar') )
    %     jheapcl;
	
    % Screen('Preference', 'SkipSyncTests', 1); % for temporary test.
	KbEventFlush( );
    
	exp_dates   = datestr(now, 'mmddyyyy');
    subjID      = input('what is subject ID: ', 's');
    sess_onset  = false;
    rnd_seed    = str2double(exp_dates)/7;
   
    %% Path define and task parameter setup
    %   Path setup.     
    paths.exc_path   = fileparts( which( mfilename ) ); cd( paths.exc_path ); add_directoryPath;
	paths.save_data  = fullfile( paths.exc_path, [ 'data/', subjID, '/', exp_dates ] );    
    if ~exist(paths.save_data, 'dir')
        mkdir(paths.save_data);
        trial_idx = 0;
    else
        cd(paths.save_data);
        n_files = dir('*.mat*');
        trial_idx = length(n_files);
        cd(paths.exc_path);
    end
    fprintf( 'trial index: %4.2d    \n', trial_idx+1);
    paths.image_data = fullfile( paths.exc_path, 'images' );    
    paths.sound_data = fullfile( paths.exc_path, 'sounds' );    
    
    %   Task parameter setup. 
    [strb_opt, vis_opt, aud_opt, exp_opt, eye_opt] = setup_variables(rnd_seed, paths);
    
    %% Looping the task 
    global stage ovr_score
    stage = 0;
    ovr_score = 0;
    
    %   How to define unattended trials?
    Screen( 'Flip', vis_opt.wndw ); % Present the screen
    clc; % For visual convenience.
    disp( 'Right Arrow to start | Escape to Exit' ); % Asking experimenter to start and exit.
    [sess_onset, time_info] = check_keys(sess_onset, exp_opt);
    time_info.session_start = GetSecs();
    
    % Run until session is onset or trial number is smaller than the maximum trial length.
    while (sess_onset) 
        switch stage
            case 0 % ITI index (less than 500 ms if possible: due to subject's attention span).
                trial_idx = trial_idx +1;
                if (trial_idx > exp_opt.len_max_tr)
                    stage = 10;
                else
                    curr_info = set_ITI(exp_opt, eye_opt, time_info, trial_idx);
                end
            case 1 
                time_info = display_empty_screen(vis_opt, exp_opt, eye_opt, time_info, trial_idx);
            case 2  % Visualize the animal movement to subjects.
                if trial_idx == 2
                    pause(0.5);
                end
                [time_info, eye_data, curr_info] = visualize_visit(strb_opt, vis_opt, exp_opt, eye_opt, curr_info, time_info, trial_idx);
            case 3  % Choice of subjects
                [choice_info, time_info, eye_data] = obtain_choice(strb_opt, vis_opt, exp_opt, eye_opt, curr_info, time_info, trial_idx);
            case 4  % Show the results
                [time_info, eye_data] = present_result(strb_opt, vis_opt, exp_opt, eye_opt, curr_info, time_info, trial_idx);
            case 5  % Show and sound feedback
                [time_info, eye_data] = present_feedback(strb_opt, vis_opt, aud_opt, exp_opt, eye_opt, choice_info, curr_info, time_info, trial_idx);
            case 6
                choice_info.obj_seq  = exp_opt.obj_seq; % Trial-by-trial what object is located in each location.
                choice_info.targ_obj = exp_opt.s_targ;  % What was the target object?
                choice_info.targ_loc = curr_info.targ_idx; % What was the target object?
                time_info = save_trial_data(paths, eye_data, choice_info, time_info, trial_idx);
            case 10 % End of trial or opting out from the session.
                sess_onset = false;
                time_info.session_end = GetSecs();
                display_final(strb_opt, vis_opt, exp_opt);
        end
    end
    
    % Winding out the experiment.
    sca;
    PsychPortAudio('Close', aud_opt.pahandle);% Close the audio device:
        
    % Convert all data into single file (like Pacman)
    save_data_whipcream(subjID, paths, exp_opt, vis_opt, trial_idx)
end

function [curr_info, time_info] = set_ITI(exp_opt, eye_opt, time_info, trial_idx) %#ok<INUSL>
    
    global stage
    
    % Set information related to the current trial.
    curr_info.animal    = exp_opt.a_idx(trial_idx);
    curr_info.house     = exp_opt.h_idx(trial_idx);
    curr_info.obj_disp  = exp_opt.obj_locs; % (exp_opt.obj_seq(trial_idx, :), :);    
    curr_info.target    = exp_opt.s_targ(trial_idx);
    curr_info.targ_idx  = find(exp_opt.obj_seq(trial_idx, :) == curr_info.target);
    curr_info.targ_obj  = curr_info.obj_disp(curr_info.targ_idx, :);
    curr_info.init_loc  = exp_opt.init_loc(trial_idx, :);
        
    % Key press check
    [time_info, trial_onset] = check_keypress_inplay(exp_opt, time_info);    
    
    % Update stage
    if ~trial_onset
        stage = 10;
    else
        stage = 1;
    end
end

function [time_info, eye_data] = display_empty_screen(vis_opt, exp_opt, eye_opt, time_info, trial_idx) %#ok<INUSL>

    global stage

    %% Show information about the upcoming trial
    %   Display of background
    if trial_idx < exp_opt.tr_init_new
        bgrnd = Screen('MakeTexture', vis_opt.wndw,  vis_opt.bgrndImg{1});
    else
        bgrnd = Screen('MakeTexture', vis_opt.wndw,  vis_opt.bgrndImg{2});
    end
    Screen('DrawTexture', vis_opt.wndw, bgrnd,[], vis_opt.wdwRect); % draw the object
    
    % Display the scores
    trial_info_str = ['Current trial number is:' num2str(trial_idx)];
    Screen('TextSize', vis_opt.wndw, vis_opt.font_size);
    DrawFormattedText(vis_opt.wndw,trial_info_str, vis_opt.cnt_x-vis_opt.scr_y/4, vis_opt.house_loc(2)-vis_opt.scr_y/4,[255 255 255]);
    disp(trial_info_str);

    Screen('Flip', vis_opt.wndw);
 
    blank_start = GetSecs();
    while GetSecs-blank_start < 1
        WaitSecs(vis_opt.wait_rate);
    end
    
    [time_info, trial_onset] = check_keypress_inplay(exp_opt, time_info);    

    % Update stage
    if ~trial_onset
        stage = 10;
    else
        stage = 2;
    end
end

function [time_info, eye_data, curr_info] = visualize_visit(strb_opt, vis_opt, exp_opt, eye_opt, curr_info, time_info, trial_idx)

    %% This function visualizes what combines to what.
    %   But it will be slightly animated.
    %   For that purpose, speed and initial location should be defined. 
    global stage
   
    if strb_opt.strobe_on
        %strobe
    end
    disp(['trial # ', num2str(trial_idx), ' display starts.']);
    time_info.disp_start = GetSecs();
    
    % Key press check (in case, we need to escape)
    [time_info, trial_onset] = check_keypress_inplay(exp_opt, time_info);    
    
    t_pts    = 0;
    curr_pos = curr_info.init_loc;
    last_pos = vis_opt.house_loc+[0, exp_opt.sensitive_r/3];
    eye_data = [];
    while trial_onset && (sqrt(sum((curr_pos-last_pos).^2)) > (exp_opt.sensitive_r/3))
        t_pts = t_pts +1;
        % Time aquistion for the looping
        disp_st_t = GetSecs();
        
        if eye_opt.eye_cam_on
            eye_track = Eyelink( 'newestfloatsample' );
        end
        
        % Display of background
        if trial_idx < exp_opt.tr_init_new
            bgrnd = Screen('MakeTexture', vis_opt.wndw,  vis_opt.bgrndImg{1});
        else
            bgrnd = Screen('MakeTexture', vis_opt.wndw,  vis_opt.bgrndImg{2});
        end
        Screen('DrawTexture', vis_opt.wndw, bgrnd,[], vis_opt.wdwRect); % draw the object
        
        % Display of house (location is stationary: I put these at the screen permanantly).
        %   image presentation rectangles (these are fixed images)
        house = Screen('MakeTexture', vis_opt.wndw,  vis_opt.houseImg{curr_info.house});
        Screen('DrawTexture', vis_opt.wndw, house,[], vis_opt.house_rect); % draw the object
        
        % Display of objects (stationary but semi-permanant meaning stationary only at trial)
        %   Do I have to loop (check at the optimization stage)?
        for iObj = 1 : exp_opt.n_scr_obj
            obj_loc  = curr_info.obj_disp(iObj, :);
            obj_rect = [obj_loc(1)-vis_opt.obj_sz/2, obj_loc(2)-vis_opt.obj_sz/2, ...
                obj_loc(1)+vis_opt.obj_sz/2, obj_loc(2)+vis_opt.obj_sz/2];
            object = Screen('MakeTexture', vis_opt.wndw,  vis_opt.objectsImg{exp_opt.obj_seq(trial_idx, iObj)});
            Screen('DrawTexture', vis_opt.wndw, object,[], obj_rect); % draw the object
        end
        
        % Move the animal and stop avatar at house (or slightly below that)
        %   It can be achieved by asking move at spd S until current position and last position matches.
        %   Another tip is that the movement only happens in x-dimension at Collin's code.
        %   Furthermore, the direction of movement is towards the right for initial movement.
        
        % Display of house (stationary: shall I put at the screen permanantly).
        animal_loc  = curr_pos;
        animal_rect = [animal_loc(1)-vis_opt.animal_sz/2, animal_loc(2)-vis_opt.animal_sz/2, ...
            animal_loc(1)+vis_opt.animal_sz/2, animal_loc(2)+vis_opt.animal_sz/2];
        animal = Screen('MakeTexture', vis_opt.wndw, vis_opt.animalImg{curr_info.animal});
        Screen('DrawTexture', vis_opt.wndw, animal,[], animal_rect); % draw the object
        
        % Movement is along the linear line to between center and the initial position.
        %   This is deterministic changes.
        if  eye_opt.eye_cam_on
            % Track the eye_data and send it to strobe.
            eye_data.init_move_eye_px( t_pts ) = eye_track.gx( eye_opt.eye.side );
            eye_data.init_move_eye_px( t_pts ) = eye_track.gy( eye_opt.eye.side );
            
            % Add pupil tracking (iso-luminance should be obtained).
        end
        
        Screen('Flip', vis_opt.wndw);
        
        [time_info, trial_onset] = check_keypress_inplay(exp_opt, time_info);
        
        % Update current position.
        curr_pos = update_position_init(curr_pos, last_pos, exp_opt);
        
        while GetSecs - disp_st_t  < (1/vis_opt.monitor_rate)
            WaitSecs(vis_opt.wait_rate);
        end
        
        % Time check (dependent on the computer).
        %         t_final = GetSecs() - disp_st_t;
        %         if t_final > round( (1/vis_opt.monitor_rate), 3)
        %             disp(['loop time: ', num2str(t_final)]);
        %         end
    end

    if strb_opt.strobe_on
        % strobe
    end    
    disp(['trial # ', num2str(trial_idx), ' animal arrived house.']);
    time_info.disp_end = GetSecs();
    curr_info.animal_loc = curr_pos;
    
    % Update stage
    if ~trial_onset
        stage = 10;
    else
        stage = 3;
    end
end

function [choice_info, time_info, eye_data] = obtain_choice(strb_opt, vis_opt, exp_opt, eye_opt, curr_info, time_info, trial_idx)

    %% This function allows to put whip cream at any of N location. 
    %   Nothing moves but only the whipcream according to selection moves. 
    %   Critical aspects: the location in between is the problem.
    %       Then just calculate the Euclidean distance and determine. 
    %       Mention this at the information to subjects. 
    global stage
    
    % Put strobe here (Strobe number should vary)
    if strb_opt.strobe_on
        strobe(strb_opt.choice_start);
    end
    
    disp(['trial # ', num2str(trial_idx), ' choice start.']);
    time_info.choice_start = GetSecs();
    eye_data = [];
    
    % Key press check (in case, we need to escape)
    [time_info, trial_onset] = check_keypress_inplay(exp_opt, time_info);
    
    %% Looping until all the whip creams are out.
    cream_rect  = vis_opt.cream_rect;
    full_cr     = 1:exp_opt.n_creams; 
    
    % Show cursor to guide them
    ShowCursor;
    
    t_pts     = 0;
    f_box_on  = false;
    cream_sel = false;
    cream_Cnt = 0; 
    [sel_cr, loc_cr, loc_obj, new_sel_cr] = replicate([]);
    [cream_sel_t, cream_loc_t, located_x, located_y] = replicate(zeros(exp_opt.n_creams, 1));
    cream_disp_t = GetSecs(); % Cream selection and location time is all according to initial cream display time. 
    while trial_onset
        t_pts = t_pts+1;
        iter_time = GetSecs();
        
        if eye_opt.eye_cam_on
            eye_track = Eyelink( 'newestfloatsample' );
        end
        
        % Key press check (in case, we need to escape)
        [time_info, trial_onset] = check_keypress_inplay(exp_opt, time_info);

        %% Visualize at the predefined location (and fixed along this stage).
        % Display of background
        if trial_idx < exp_opt.tr_init_new
            bgrnd = Screen('MakeTexture', vis_opt.wndw,  vis_opt.bgrndImg{1});
        else
            bgrnd = Screen('MakeTexture', vis_opt.wndw,  vis_opt.bgrndImg{2});
        end
        Screen('DrawTexture', vis_opt.wndw, bgrnd,[], vis_opt.wdwRect); % draw the object
        
        % Display of house (location is stationary: I put these at the screen permanantly).
        %   image presentation rectangles (these are fixed images)
        house = Screen('MakeTexture', vis_opt.wndw,  vis_opt.houseImg{curr_info.house});
        Screen('DrawTexture', vis_opt.wndw, house,[], vis_opt.house_rect); % draw the object
        
        % Display of objects (stationary but semi-permanant meaning stationary only at trial)
        %   Do I have to loop (check at the optimization stage)?
        for iObj = 1 : exp_opt.n_scr_obj
            obj_loc  = curr_info.obj_disp(iObj, :);
            obj_rect = [obj_loc(1)-vis_opt.obj_sz/2, obj_loc(2)-vis_opt.obj_sz/2, ...
                obj_loc(1)+vis_opt.obj_sz/2, obj_loc(2)+vis_opt.obj_sz/2];
            object = Screen('MakeTexture', vis_opt.wndw,  vis_opt.objectsImg{exp_opt.obj_seq(trial_idx, iObj)});
            Screen('DrawTexture', vis_opt.wndw, object,[], obj_rect); % draw the object
        end

        % Display of house (stationary: shall I put at the screen permanantly).
        animal_loc  = curr_info.animal_loc;
        animal_rect = [animal_loc(1)-vis_opt.animal_sz/2, animal_loc(2)-vis_opt.animal_sz/2, ...
            animal_loc(1)+vis_opt.animal_sz/2, animal_loc(2)+vis_opt.animal_sz/2];
        animal = Screen('MakeTexture', vis_opt.wndw, vis_opt.animalImg{curr_info.animal});
        Screen('DrawTexture', vis_opt.wndw, animal,[], animal_rect); % draw the object
        
        % Display finish button
        if cream_Cnt > 0 && (f_box_on)
            Screen('FillRect', vis_opt.wndw, vis_opt.finish_box, vis_opt.finish_rect);
            Screen('DrawText', vis_opt.wndw, 'Finish', vis_opt.textX, vis_opt.textY, vis_opt.text_col);            
        end        
        
        %% Get mouse position at any given time point.
        [x_loc, y_loc, button] = GetMouse(vis_opt.wndw);

        if ~cream_sel && button(1) % cream was not select but left button was pressed.
            % Check whether the clicks for the cream selection were made in any weird place.
            %   step 1. Calculate the distance with respect to whip cream.
            x_derv_cream    = (x_loc - vis_opt.cream_loc(:, 1));
            y_derv_cream    = (y_loc - vis_opt.cream_loc(:, 2));
            dist_to_cream   = calculate_distance(x_derv_cream, y_derv_cream); % Three distance is calculated.
            
            % step 2. Check whether mouse cursor is within range of any whip cream.
            check_dist_cream = dist_to_cream < exp_opt.sensitive_r;
            
            if cream_Cnt > 0
                % Temporary step. Opt out after putting down a cream (only
                % avaiable option after putting down one cream)
                x_derv_finish   = (x_loc - vis_opt.f_box_loc(:, 1));
                y_derv_finish   = (y_loc - vis_opt.f_box_loc(:, 2));
                dist_finish     = calculate_distance(x_derv_finish, y_derv_finish);
                
                % Check whether the button press is at the finish button.
                %   Some limitation for the button size (of course, can change the code but I am just lazy).
                %   The finish button is locate at the opposite side of the cream location.
                check_dist_finish = (dist_finish<exp_opt.sensitive_r)&&sum(dist_finish<dist_to_cream)&&(cream_Cnt>0);
            else
                check_dist_finish = false;
            end
            
            % step 3. Determine whether the cream was correctly selected
            if (~check_dist_finish)
                if (sum(check_dist_cream) >= 1)
                    if sum(check_dist_cream) > 0  && sum(check_dist_cream) < 2
                        % only one whip cream is within the range.
                        new_sel_cr = find(check_dist_cream == 1);
                    elseif sum(check_dist_cream) >= 2
                        % more than one whip cream is within the range.
                        [~, new_sel_cr] = min(dist_to_cream);
                    end
                    sel_cr = [sel_cr, new_sel_cr];
                    
                    % Put strobe here
                    if strb_opt.strobe_on
                        strobe(strb_opt.cream_select);
                    end
                    
                    % Step 4. Update information
                    cream_sel = true;
                    cream_Cnt = cream_Cnt +1; % Update the number of located cream_Cnt.
                    cream_sel_t(cream_Cnt, 1) = GetSecs()-cream_disp_t;
                end
                
                if cream_Cnt >= 1
                    f_box_on = true;
                end
            else
                % Exit the loop.
                trial_onset = false;
                f_box_on    = false;
            end
        elseif cream_sel && button(2) % When cream was selected and right button was clicked
            % Check whether the clicks for the cream location were made in any weird place.
            %   step 1. Calculate the distance with respect to object.
            x_derv_obj  = (x_loc - curr_info.obj_disp(:, 1));
            y_derv_obj  = (y_loc - curr_info.obj_disp(:, 2));
            dist_to_obj = calculate_distance(x_derv_obj, y_derv_obj); % Three distance is calculated.
 
            % step 2. Check whether mouse cursor is within range of any object.
            check_dist_obj = dist_to_obj < exp_opt.sensitive_r;
            
            % step 3. Determine whether the cream was correctly located
            if (sum(check_dist_obj) >= 1)
                if sum(check_dist_obj) > 0  && sum(check_dist_obj) < 2
                    % only one whip cream is within the range.
                    loc_obj = [loc_obj, find(check_dist_obj == 1)];
                elseif sum(check_dist_obj) >= 2
                    % more than one whip cream is within the range.
                    [~, min_obj] = min(dist_to_obj);
                    loc_obj = [loc_obj, min_obj];
                end
                loc_cr = [loc_cr, new_sel_cr]; % Maintain sequence information (for saving data)
                
                % Save location of 'located cream'
                %   It can be always identical location with respect to certain object.
                located_x(new_sel_cr) = x_loc;
                located_y(new_sel_cr) = y_loc;
                
                % Put strobe here
                if strb_opt.strobe_on
                    strobe(strb_opt.cream_locate);
                end
                
                % Step 4. Update information
                new_sel_cr = [];
                disp( [ num2str(cream_Cnt), ' cream has been located']);
                cream_sel  = false; % Logic of this argument name: since located cream, now subject has no cream selected.
                cream_loc_t(cream_Cnt, 1) = GetSecs()-cream_disp_t;
                
                % exit the loop.
                if cream_Cnt == exp_opt.n_creams
                    trial_onset = false;
                end
            end            
        end
        
        % Define and display unselected cream.
        %   If nothing is selected and located, full cream length would be
        %   identical to maximum number of creams.
        rem_cr = setdiff(full_cr, unique([loc_cr, new_sel_cr]));
        for iRCr = rem_cr
            % change the location of selected
            cream = Screen('MakeTexture', vis_opt.wndw,  vis_opt.creamImg{1});
            Screen('DrawTexture', vis_opt.wndw, cream,[], cream_rect(iRCr, :)); % draw the object
        end
        
        % Display already located cream
        for iLCr = loc_cr
            cream_rect(iLCr, :) = [located_x(iLCr)-vis_opt.cream_sz/2, located_y(iLCr)-vis_opt.cream_sz/2,...
                located_x(iLCr)+vis_opt.cream_sz/2, located_y(iLCr)+vis_opt.cream_sz/2];
            
            % change the location of selected
            cream = Screen('MakeTexture', vis_opt.wndw,  vis_opt.creamImg{1});
            Screen('DrawTexture', vis_opt.wndw, cream,[], cream_rect(iLCr, :)); % draw the object
        end        
        
        % Display currently selected cream
        %   Location information is dynamically changing.
        for iSCr = new_sel_cr
            cream_rect(iSCr, :) = [x_loc-vis_opt.cream_sz/2, y_loc-vis_opt.cream_sz/2,...
                x_loc+vis_opt.cream_sz/2, y_loc+vis_opt.cream_sz/2];
            cream = Screen('MakeTexture', vis_opt.wndw,  vis_opt.creamImg{1});
            Screen('DrawTexture', vis_opt.wndw, cream,[], cream_rect(iSCr, :)); % draw the object
        end
        
        if  eye_opt.eye_cam_on
            % Track the eye_data and send it to strobe.
            eye_data.choice_eye_px(t_pts) = eye_track.gx( eye_opt.eye.side );
            eye_data.choice_eye_px(t_pts) = eye_track.gy( eye_opt.eye.side );
            
            % Add pupil tracking (iso-luminance should be obtained).
        end
        
        % draw new location for selected cream.
        Screen('Flip', vis_opt.wndw);
    
        while GetSecs()-iter_time < (1/vis_opt.monitor_rate) 
            WaitSecs(vis_opt.wait_rate);
        end
    end
    disp(['trial # ', num2str(trial_idx), ' choice end.']);
    
    % Put strobe here
    if strb_opt.strobe_on
        strobe(strb_opt.choice_end);
    end
    
    % See what information to save.
    choice_info.loc_obj     = loc_obj;
    time_info.cream_loc_t   = cream_loc_t;
    time_info.choice_end    = GetSecs();
    
    % Update stage
    if stage ~= 10
        stage = 4;
    end
end

function [time_info, eye_data] = present_result(strb_opt, vis_opt, exp_opt, eye_opt, curr_info, time_info, trial_idx)

    %% This function shows the trajectory actually the animal takes. 
    %   No choice involved.
    %   Half of the time, it moves to selected location.
    %   Rest half of the time, it moves back to initial location.
    global stage

    % Put strobe here
    if strb_opt.strobe_on
        strobe(strb_opt.result_present);
    end
    disp(['trial # ', num2str(trial_idx), ' result present.']);
    time_info.result_present_start = GetSecs();
    eye_data=[];
    
    % Key press check (in case, we need to escape)
    [time_info, trial_onset] = check_keypress_inplay(exp_opt, time_info);    
    
    curr_pos  = vis_opt.house_loc;
    last_pos  = curr_info.targ_obj;
    
    t_pts = 0;
    reached_obj = false; reached_house = false;
    reach_idx = 1;
    while trial_onset && ((~reached_obj) || (~reached_house))
        t_pts = t_pts+1;
        % Time aquistion for the looping
        disp_st_t = GetSecs();
        
        if eye_opt.eye_cam_on
            eye_track = Eyelink( 'newestfloatsample' );
        end
        
        % Display of background
        if trial_idx < exp_opt.tr_init_new
            bgrnd = Screen('MakeTexture', vis_opt.wndw,  vis_opt.bgrndImg{1});
        else
            bgrnd = Screen('MakeTexture', vis_opt.wndw,  vis_opt.bgrndImg{2});
        end
        Screen('DrawTexture', vis_opt.wndw, bgrnd,[], vis_opt.wdwRect); % draw the object
        
        % Display of house (location is stationary: I put these at the screen permanantly).
        %   image presentation rectangles (these are fixed images)
        house = Screen('MakeTexture', vis_opt.wndw,  vis_opt.houseImg{curr_info.house});
        Screen('DrawTexture', vis_opt.wndw, house,[], vis_opt.house_rect); % draw the object
        
        % Display of objects (stationary but semi-permanant meaning stationary only at trial)
        %   Do I have to loop (check at the optimization stage)?
        for iObj = 1 : exp_opt.n_scr_obj
            obj_loc  = curr_info.obj_disp(iObj, :);
            obj_rect = [obj_loc(1)-vis_opt.obj_sz/2, obj_loc(2)-vis_opt.obj_sz/2, ...
                obj_loc(1)+vis_opt.obj_sz/2, obj_loc(2)+vis_opt.obj_sz/2];
            object = Screen('MakeTexture', vis_opt.wndw,  vis_opt.objectsImg{exp_opt.obj_seq(trial_idx, iObj)});
            Screen('DrawTexture', vis_opt.wndw, object,[], obj_rect); % draw the object
        end
        
        % Move the animal and stop avatar at object first (or slightly below that)
        %   It can be achieved by asking move at spd S until current position and last position matches.       
        animal_loc  = curr_pos;
        animal_rect = [animal_loc(1)-vis_opt.animal_sz/2, animal_loc(2)-vis_opt.animal_sz/2, ...
            animal_loc(1)+vis_opt.animal_sz/2, animal_loc(2)+vis_opt.animal_sz/2];
        animal = Screen('MakeTexture', vis_opt.wndw, vis_opt.animalImg{curr_info.animal});
        Screen('DrawTexture', vis_opt.wndw, animal,[], animal_rect); % draw the object
        
        % Movement is along the linear line to between center and the initial position.
        %   This is deterministic changes.
        if  eye_opt.eye_cam_on
            % Track the eye_data and send it to strobe.
            eye_data.after_eye_px(t_pts) = eye_track.gx( eye_opt.eye.side );
            eye_data.after_eye_px(t_pts) = eye_track.gy( eye_opt.eye.side );
            
            % Add pupil tracking (iso-luminance should be obtained).
        end
        Screen('Flip', vis_opt.wndw);

        x_dervs  = (last_pos(1) - curr_pos(1));
        y_dervs  = (last_pos(2) - curr_pos(2));
        dist_to_targ = calculate_distance(x_dervs, y_dervs); % Three distance is calculated.
        
        % step 2. Check whether mouse cursor is within range of any object.
        check_dist = dist_to_targ < exp_opt.criteria;
 
        if check_dist 
            if reach_idx == 1
                reached_obj = true;
                reach_idx   = reach_idx+1;
                last_pos    = vis_opt.house_loc;
            elseif reach_idx == 2
                reached_house = true;
            else
                disp('something wrong. please check.')                
            end
        else
            % Update current position.
            curr_pos = update_position(curr_pos, last_pos, exp_opt);
        end

        % Key press check (in case, we need to escape)
        [time_info, trial_onset] = check_keypress_inplay(exp_opt, time_info);
        
        while GetSecs - disp_st_t  < (1/vis_opt.monitor_rate)
            WaitSecs(vis_opt.wait_rate);
        end
        
        % Time check (dependent on the computer).
        %         t_final = GetSecs() - disp_st_t;
        %         if t_final > round( (1/vis_opt.monitor_rate), 3)
        %             disp(['loop time: ', num2str(t_final)]);
        %         end
    end
    disp(['trial # ', num2str(trial_idx), ' result presentation ends.']);
    time_info.result_present_end = GetSecs();
    
    % Update stage
    if ~trial_onset
        stage = 10;
    else
        stage = 5;
    end
end

function [time_info, eye_data] = present_feedback(strb_opt, vis_opt, aud_opt, exp_opt, eye_opt, choice_info, curr_info, time_info, trial_idx)

    %% This function calculates the current outcome and global outcome.
    % How was the score calculated (I need to be reminded). 
    global stage ovr_score
    
    % Put strobe here
    if strb_opt.strobe_on
        strobe(strb_opt.fdb_present);
    end    
    disp(['trial # ', num2str(trial_idx), ' feedback present.']);
    time_info.feedback_present_start = GetSecs();
    
    eye_data = [];
    if eye_opt.eye_cam_on
        eye_track = Eyelink( 'newestfloatsample' );
    end
    % Key press check (in case, we need to escape)
    [time_info, ~] = check_keypress_inplay(exp_opt, time_info);    
    
    %% Scoring system goes in here
    curr_score = sum(choice_info.loc_obj == curr_info.targ_idx);
    ovr_score  = ovr_score+curr_score;
    
    %% Show visual
    t_pts = 0;
    score_disp = GetSecs();
    while GetSecs()-score_disp < exp_opt.score_t
        single_loop_t = GetSecs();
        t_pts = t_pts+1;
        % Display of background
        if trial_idx < exp_opt.tr_init_new
            bgrnd = Screen('MakeTexture', vis_opt.wndw,  vis_opt.bgrndImg{1});
        else
            bgrnd = Screen('MakeTexture', vis_opt.wndw,  vis_opt.bgrndImg{2});
        end
        Screen('DrawTexture', vis_opt.wndw, bgrnd,[], vis_opt.wdwRect); % draw the object
        
        % Display of house (location is stationary: I put these at the screen permanantly).
        %   image presentation rectangles (these are fixed images)
        house = Screen('MakeTexture', vis_opt.wndw,  vis_opt.houseImg{curr_info.house});
        Screen('DrawTexture', vis_opt.wndw, house,[], vis_opt.house_rect); % draw the object
        
        % Display of objects (stationary but semi-permanant meaning stationary only at trial)
        %   Do I have to loop (check at the optimization stage)?
        for iObj = 1 : exp_opt.n_scr_obj
            obj_loc  = curr_info.obj_disp(iObj, :);
            obj_rect = [obj_loc(1)-vis_opt.obj_sz/2, obj_loc(2)-vis_opt.obj_sz/2, ...
                obj_loc(1)+vis_opt.obj_sz/2, obj_loc(2)+vis_opt.obj_sz/2];
            object = Screen('MakeTexture', vis_opt.wndw,  vis_opt.objectsImg{exp_opt.obj_seq(trial_idx, iObj)});
            Screen('DrawTexture', vis_opt.wndw, object,[], obj_rect); % draw the object
        end
        
        % Move the animal and stop avatar at object first (or slightly below that)
        %   It can be achieved by asking move at spd S until current position and last position matches.
        animal_loc  = vis_opt.house_loc+[0, exp_opt.sensitive_r/3];
        animal_rect = [animal_loc(1)-vis_opt.animal_sz/2, animal_loc(2)-vis_opt.animal_sz/2, ...
            animal_loc(1)+vis_opt.animal_sz/2, animal_loc(2)+vis_opt.animal_sz/2];
        animal = Screen('MakeTexture', vis_opt.wndw, vis_opt.animalImg{curr_info.animal});
        Screen('DrawTexture', vis_opt.wndw, animal,[], animal_rect); % draw the object
        
        % Display the scores
        cs_str = ['Current score is:' num2str(curr_score)];
        Screen('TextSize', vis_opt.wndw, vis_opt.font_size);
        DrawFormattedText(vis_opt.wndw,cs_str, vis_opt.cnt_x-vis_opt.scr_y/4, vis_opt.house_loc(2)-vis_opt.scr_y/4,[255 255 255]);
        
        ovr_score_str = ['Cumulative score is:' num2str(ovr_score)];
        Screen('TextSize', vis_opt.wndw, vis_opt.font_size);
        DrawFormattedText(vis_opt.wndw, ovr_score_str, vis_opt.cnt_x-vis_opt.scr_y/4, vis_opt.house_loc(2)-vis_opt.scr_y/8,[255 255 255]);
        
        % Movement is along the linear line to between center and the initial position.
        %   This is deterministic changes.
        if  eye_opt.eye_cam_on && t_pts > 1
            % Track the eye_data and send it to strobe.
            eye_data.after_eye_px(t_pts) = eye_track.gx( eye_opt.eye.side );
            eye_data.after_eye_px(t_pts) = eye_track.gy( eye_opt.eye.side );
            
            % Add pupil tracking (iso-luminance should be obtained).
        end
        
        if t_pts == 1
            if aud_opt.sound_on
                % Sound presentation comes here
                PsychPortAudio('Start', aud_opt.pahandle, aud_opt.repetitions, 0); % starts sound immediatley
                
                audio_play_t = GetSecs();
                while GetSecs()-audio_play_t < aud_opt.feedback_t
                    WaitSecs(vis_opt.wait_rate);
                end
                PsychPortAudio('Stop', aud_opt.pahandle); % Stop sound playback
            else
                while GetSecs()-single_loop_t < (1/vis_opt.monitor_rate)
                    WaitSecs(vis_opt.wait_rate);
                end
            end
        else
            while GetSecs()-single_loop_t < (1/vis_opt.monitor_rate)
                WaitSecs(vis_opt.wait_rate);
            end
        end
        [time_info, trial_onset] = check_keypress_inplay(exp_opt, time_info);
        Screen('Flip', vis_opt.wndw);
    end
        
    % Put strobe here
    if strb_opt.strobe_on
        strobe(strb_opt.fdb_ends);
    end
    disp(cs_str);
    disp(ovr_score_str);
    disp(['trial # ', num2str(trial_idx), ' feedback presentation ends.']);

    % Key press check (in case, we need to escape)
    time_info.result_present_end = GetSecs();

    % Update stage
    if ~trial_onset
        stage = 10;
    else
        stage = 6;
    end
    
    clc; % Clear after every trial. 
end

function display_final(strb_opt, vis_opt, exp_opt)

    %% This function aims to draw a rule after the final trial
    
    % Can add strobe if there is any room for that. 
    
    % NOTE) Proper location for the text needs to be adjusted. 
    %   final_text = 'The connection between animal and house is ';
    %   Screen('DrawText', vis_opt.wndw, final_text, vis_opt.scr_x-vis_opt.scr_x*(1/6), vis_opt.scr_y-vis_opt.scr_x*(1/6), vis_opt.text_col);
    
    for iH = 1 : exp_opt.n_house
        h_idx  = exp_opt.orig_h_o_map(iH, 1);
        house_final = Screen('MakeTexture', vis_opt.wndw,  vis_opt.houseImg{h_idx});
        Screen('DrawTexture', vis_opt.wndw, house_final,[], vis_opt.final_h_rect(iH, :)); % draw the object

        ho_idx = exp_opt.orig_h_o_map(iH, 2);
        ho_final = Screen('MakeTexture', vis_opt.wndw,  vis_opt.objectsImg{ho_idx});
        Screen('DrawTexture', vis_opt.wndw, ho_final,[], vis_opt.final_h_obj_rect(iH, :)); % draw the object
    end

    for iA = 1 : exp_opt.n_animal
        a_idx  = exp_opt.orig_a_o_map(iA, 1);
        animal_final = Screen('MakeTexture', vis_opt.wndw,  vis_opt.animalImg{a_idx});
        Screen('DrawTexture', vis_opt.wndw, animal_final,[], vis_opt.final_a_rect(iA, :)); % draw the object

        ao_idx = exp_opt.orig_a_o_map(iA, 2);
        ao_final = Screen('MakeTexture', vis_opt.wndw,  vis_opt.objectsImg{ao_idx});
        Screen('DrawTexture', vis_opt.wndw, ao_final,[], vis_opt.final_a_obj_rect(iA, :)); % draw the object
    end
    Screen('Flip', vis_opt.wndw);
    
    KbEventFlush( );
    
    %% Check whether key is pressed.
    [keyPressed] = KbCheck( );
    while ~(keyPressed)
        [keyPressed] = KbCheck( );
        
        if (keyPressed)
            disp('game end');
            break;
        end
    end
end