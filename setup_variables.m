function [strb_opt, vis_opt, aud_opt, exp_opt, eye_opt] = setup_variables(rnd_seed, paths)

    %% Current function is to setup variables.
    %   Later, the experimenter can flexibly modify the variables from here. 
    
    %% Variable 1. Strobes (e.g. make it adjustable to Neuralynx system).
    %   NOTE) Track the strobe system: NI-6501 (USB) acquisition system. 
    %   Then the strobe can be easily adaptable from the primate rig. 
    %   Due to the limitation in number that can be generated from the Neuralynx strobe system, 
    %       the number is limited from 0 to 127 as of 12-14-2019.
    %   Naturally, the number of trials at each session would be 120 trials.
    strb_opt.strobe_on      = false;
    strb_opt.session_start  = 121;
    strb_opt.session_end    = 122;
    strb_opt.trial_start    = 123;
    strb_opt.trial_end      = 124;    
    strb_opt.start_ITI      = 125;
    strb_opt.end_ITI        = 126;
    strb_opt.exit_key_press = 127;
     
    %% Variable 2. Visual opt
    %   Includes both device and color (e.g. resoultion and refresh rate)
    vis_opt.font_size   = 18;
    vis_opt.bg_col      = [50, 50, 50];
    vis_opt.text_col    = [0, 0, 0];
    vis_opt.finish_box  = [125, 125, 125];
    vis_opt.screen = 0;
    [vis_opt.wndw, vis_opt.wdwRect] = Screen('OpenWindow', vis_opt.screen, vis_opt.bg_col, [100, 100, 600, 400]);
    [vis_opt.scr_x, vis_opt.scr_y]  = Screen('WindowSize', vis_opt.wndw);
    vis_opt.monitor_rate = 60; % Screen('FrameRate', vis_opt.wndw);
	vis_opt.wait_rate = 1/5000;
    [vis_opt.cnt_x, vis_opt.cnt_y]  = RectCenter(vis_opt.wdwRect);
    vis_opt.max_scr_dist = sqrt(vis_opt.scr_x^2 + vis_opt.scr_y^2); 
    
    vis_opt.cream_sz     = min(vis_opt.scr_x, vis_opt.scr_y)*(1/16); % Size of the whip cream image at the screen.
    vis_opt.animal_sz    = min(vis_opt.scr_x, vis_opt.scr_y)*(1/12);
    vis_opt.obj_sz       = min(vis_opt.scr_x, vis_opt.scr_y)*(1/12);
    vis_opt.house_sz     = min(vis_opt.scr_x, vis_opt.scr_y)*(1/12);
    
    vis_opt.house_loc   = [vis_opt.cnt_x, vis_opt.cnt_y+vis_opt.cnt_y*(1/2)]; % House are slightly below from the center.
    vis_opt.house_rect  = [vis_opt.house_loc(1)-vis_opt.house_sz/2, vis_opt.house_loc(2)-vis_opt.house_sz/2, ...
        vis_opt.house_loc(1)+vis_opt.house_sz/2, vis_opt.house_loc(2)+vis_opt.house_sz/2];
    
    % Can replace with some image as well. 
    vis_opt.f_box_loc   = [vis_opt.house_loc(1), vis_opt.house_loc(2)-vis_opt.cnt_y*(1/4)];
    vis_opt.finish_rect = [vis_opt.f_box_loc(1)-vis_opt.scr_x/12, vis_opt.f_box_loc(2)-vis_opt.scr_y/18, ...
        vis_opt.f_box_loc(1)+vis_opt.scr_x/12, vis_opt.f_box_loc(2)+vis_opt.scr_y/18];
    
    vis_opt.buffer = [vis_opt.scr_x*(1/21), vis_opt.scr_y*(1/25)];
    vis_opt.textX  = vis_opt.f_box_loc(1)-vis_opt.buffer(1);
    vis_opt.textY  = vis_opt.f_box_loc(2)-vis_opt.buffer(2);
        
    % Warning from psychtoolbox off. 
    Screen('Preference', 'SuppressAllWarnings', vis_opt.screen);
    Screen('Preference', 'Verbosity', vis_opt.screen); % Hides PTB Warnings
    
    %% Variable 3. Audio opt (optional)
    %   NOTE) Match the sound with the Colin's task. 
    aud_opt.repetitions = 1; % how many repititions of the sound  
    InitializePsychSound(1); % inidializes sound driver...the 1 pushes for low latency
    clap_audio = [paths.sound_data, '/clapSound.mp3'];
    [aud_opt.wavedata, aud_opt.freq] = audioread(clap_audio); % load sound file (make sure that it is in the same folder as this script    
    aud_opt.pahandle = PsychPortAudio('Open', [], [], 2, aud_opt.freq/2, 1, 0); % opens sound buffer at a different frequency
    PsychPortAudio('FillBuffer', aud_opt.pahandle, aud_opt.wavedata(:, 1)'); % loads data into buffer        
    PsychPortAudio('Start', aud_opt.pahandle, aud_opt.repetitions, 0); % starts sound immediatley
    PsychPortAudio('Stop', aud_opt.pahandle); % Stop sound playback
    aud_opt.feedback_t = 0.5;
    aud_opt.sound_on = false;
    
    %% Variable 4. Task variables.
    exp_opt.len_max_tr  = 3; % Default = 120
    exp_opt.tr_init_new = 2;  % Among maximum number of trials, when does new animal and object being introduced? (default = 75);
    exp_opt.n_animal    = 4;
    exp_opt.new_animal  = 1; % How many new animals are introduced?
    exp_opt.n_house     = 3;
    exp_opt.new_house   = 1;
    exp_opt.n_object    = 4; % This tells how many objects(desserts) are at the pool. 
    exp_opt.new_object  = 1; % How many new objects(desserts) are intoduced?
    exp_opt.n_scr_obj   = 3; % This tells how many objects(desserts) are shown at the screen. 
    exp_opt.n_creams    = exp_opt.n_scr_obj; % Match the maximum number of creams. 
    exp_opt.seg_deg     = 360/exp_opt.n_animal;
    exp_opt.dist_obj    = vis_opt.max_scr_dist/4; % Relative distance for different resolution of the monitor. 
    exp_opt.move_spd    = [vis_opt.scr_x, vis_opt.scr_y]/200; % Movement speed: 'pixel/refresh rate'
    exp_opt.score_t     = 2;
    
    % Load all the images (objects will be abstract shape).
    %   Consideration) It will be easier to load images than drawing the
    %   shape. Consider that aspect. 
    vis_opt.animalImg   = load_images(fullfile(paths.image_data, 'animal'), (exp_opt.n_animal+exp_opt.new_animal));
    vis_opt.houseImg    = load_images(fullfile(paths.image_data, 'house'), (exp_opt.n_house+exp_opt.new_house));
    vis_opt.objectsImg  = load_images(fullfile(paths.image_data, 'object'), (exp_opt.n_object+exp_opt.new_object));
    vis_opt.creamImg    = load_images(fullfile(paths.image_data, 'tools'), 1);
    vis_opt.bgrndImg    = load_images(fullfile(paths.image_data, 'background'), 2);
    cd(paths.exc_path);
    
    mat_cream = [linspace(-1.75, 1.75, exp_opt.n_creams)', zeros(exp_opt.n_creams, 1)]*vis_opt.cream_sz; 
    vis_opt.cream_loc = repmat([vis_opt.cnt_x*1.5, vis_opt.cream_sz*1.25], exp_opt.n_creams, 1)+mat_cream;  % Located at the x-center and 1/8 of Y.
    
    for iCr = 1 : exp_opt.n_creams
        vis_opt.cream_rect(iCr, :)  = [vis_opt.cream_loc(iCr, 1)-vis_opt.cream_sz/2, vis_opt.cream_loc(iCr, 2)-vis_opt.cream_sz/2, ...
            vis_opt.cream_loc(iCr, 1)+vis_opt.cream_sz/2, vis_opt.cream_loc(iCr, 2)+vis_opt.cream_sz/2];
    end
    
    vis_opt.final_loc_animal = [repmat(vis_opt.cnt_x-7*vis_opt.house_sz, exp_opt.n_animal, 1), ...
        vis_opt.cnt_y+vis_opt.cnt_y*linspace(-0.3, 0.3, exp_opt.n_animal)'];
    vis_opt.final_loc_house = [repmat(vis_opt.cnt_x+4*vis_opt.house_sz, exp_opt.n_house, 1), ...
        vis_opt.cnt_y+vis_opt.cnt_y*linspace(-0.3, 0.3, exp_opt.n_house)'];
    vis_opt.final_loc_a_obj = [repmat(vis_opt.cnt_x-4*vis_opt.house_sz, exp_opt.n_animal, 1), ...
        vis_opt.cnt_y+vis_opt.cnt_y*linspace(-0.3, 0.3, exp_opt.n_animal)'];
    vis_opt.final_loc_h_obj = [repmat(vis_opt.cnt_x+7*vis_opt.house_sz, exp_opt.n_house, 1), ...
        vis_opt.cnt_y+vis_opt.cnt_y*linspace(-0.3, 0.3, exp_opt.n_house)'];

    for iA = 1 : exp_opt.n_animal
        vis_opt.final_a_rect(iA, :)  = [vis_opt.final_loc_animal(iA, 1)-vis_opt.animal_sz/2, vis_opt.final_loc_animal(iA, 2)-vis_opt.animal_sz/2, ...
            vis_opt.final_loc_animal(iA, 1)+vis_opt.animal_sz/2, vis_opt.final_loc_animal(iA, 2)+vis_opt.animal_sz/2];
        vis_opt.final_a_obj_rect(iA, :)  = [vis_opt.final_loc_a_obj(iA, 1)-vis_opt.animal_sz/2, vis_opt.final_loc_a_obj(iA, 2)-vis_opt.animal_sz/2, ...
            vis_opt.final_loc_a_obj(iA, 1)+vis_opt.animal_sz/2, vis_opt.final_loc_a_obj(iA, 2)+vis_opt.animal_sz/2];    
    end
    
    for iH = 1 : exp_opt.n_house
        vis_opt.final_h_rect(iH, :)  = [vis_opt.final_loc_house(iH, 1)-vis_opt.house_sz/2, vis_opt.final_loc_house(iH, 2)-vis_opt.house_sz/2, ...
            vis_opt.final_loc_house(iH, 1)+vis_opt.house_sz/2, vis_opt.final_loc_house(iH, 2)+vis_opt.house_sz/2];    
        vis_opt.final_h_obj_rect(iH, :)  = [vis_opt.final_loc_h_obj(iH, 1)-vis_opt.house_sz/2, vis_opt.final_loc_h_obj(iH, 2)-vis_opt.house_sz/2, ...
            vis_opt.final_loc_h_obj(iH, 1)+vis_opt.house_sz/2, vis_opt.final_loc_h_obj(iH, 2)+vis_opt.house_sz/2];    
    end
    
    % The score system: identical points for each creasm
    exp_opt.scores = 10*[exp_opt.n_creams:-1:1]; % Less score you get by putting more whip creams. 
    
    % Now, how to make each cases:
    %   1) One object - one animal mapping.
    %   2) One object - multiple animal mapping.
    %   Still, there is a possibility that there is no mapping between the animal and object.
    %   Also, the possibility of overlapping target between animals (or houses) are possible. 
    rng(rnd_seed);
    if exp_opt.n_animal >= exp_opt.n_object
        exp_opt.animal_obj_map = [1:exp_opt.n_animal; datasample(1:exp_opt.n_object, exp_opt.n_animal)]';
    else
        exp_opt.animal_obj_map = [1:exp_opt.n_animal; datasample(1:exp_opt.n_animal, exp_opt.n_animal)]';        
    end
    exp_opt.orig_a_o_map = exp_opt.animal_obj_map;
    
    if exp_opt.n_house > exp_opt.n_object
        exp_opt.house_obj_map = [1:exp_opt.n_house; datasample(1:exp_opt.n_object, exp_opt.n_house)]';       
    else
        exp_opt.house_obj_map = [1:exp_opt.n_house; datasample(1:exp_opt.n_house, exp_opt.n_house)]';                
    end
    exp_opt.orig_h_o_map = exp_opt.house_obj_map;
    
    % How to make a trial sequence (randomized) and consider also two phase. 
    [exp_opt.a_idx, exp_opt.h_idx, exp_opt.s_targ] = replicate(cell(2, 1));
    for i = 1 : 2
        if i == 1 % Introducting new object part.
            n_house  = exp_opt.n_house;
            n_animal = exp_opt.n_animal;
            l_tr     = exp_opt.tr_init_new;
            [exp_opt.a_idx{i}, exp_opt.h_idx{i}, exp_opt.s_targ{i}] = ...
                determine_combination(rnd_seed, l_tr, n_animal, n_house, exp_opt.animal_obj_map, exp_opt.house_obj_map);
        elseif i == 2
            n_house  = exp_opt.n_house+exp_opt.new_house;
            n_animal = exp_opt.n_animal+exp_opt.new_animal;
            
            % Extend animal_obj_map, house_obj_map
            exp_opt.animal_obj_map = [exp_opt.animal_obj_map; exp_opt.n_animal+[1:exp_opt.new_animal], datasample(1:exp_opt.n_object, exp_opt.new_animal)];
            exp_opt.house_obj_map  = [exp_opt.house_obj_map; exp_opt.n_house+[1:exp_opt.new_house], datasample(1:exp_opt.n_object, exp_opt.new_house)];
           
            l_tr     = exp_opt.len_max_tr - exp_opt.tr_init_new;
            [exp_opt.a_idx{i}, exp_opt.h_idx{i}, exp_opt.s_targ{i}] = ...
                determine_combination(rnd_seed, l_tr, n_animal, n_house, exp_opt.animal_obj_map, exp_opt.house_obj_map);
        end
    end
    exp_opt.a_idx = cell2mat(exp_opt.a_idx);
    exp_opt.h_idx = cell2mat(exp_opt.h_idx);
    exp_opt.s_targ = cell2mat(exp_opt.s_targ);
    
    % Randomized location of the objects at each trial.
    exp_opt.obj_seq = zeros(exp_opt.len_max_tr, exp_opt.n_scr_obj);
    for iTr = 1 : exp_opt.len_max_tr
        rem_obj = setdiff([1:exp_opt.n_object], exp_opt.s_targ(iTr));
        % Must include target object.
        %   However, the pool of object can be larger than pool of animal or target.
        tr_objs = [exp_opt.s_targ(iTr), datasample(rem_obj, exp_opt.n_scr_obj-1, 'replace', false)]; 
        exp_opt.obj_seq(iTr, :) = datasample(tr_objs, length(tr_objs), 'replace', false);
    end
    
    % Location of each object with respect to number of the maximum object.
    % Questionable aspect: whether new object make visual changes in object location?
    ang_unit = 180/(exp_opt.n_scr_obj);  %#ok<NASGU>
    angs     = 360-linspace(30, 150, exp_opt.n_scr_obj);
    exp_opt.orig_vect = [exp_opt.dist_obj, 0]; % vis_opt.house_loc + [0, exp_opt.dist_obj];

    exp_opt.obj_locs = zeros(exp_opt.n_scr_obj, 2);
    for iN = 1 : exp_opt.n_scr_obj
        rot_mat  = [cosd(angs(iN)), -sind(angs(iN)); sind(angs(iN)), cosd(angs(iN))];
        exp_opt.obj_locs(iN, :) = vis_opt.house_loc+(rot_mat*exp_opt.orig_vect')';
    end
    
    % Initial location is randomized. 
    %   For now, come from right or left. 
    rot_ang = [0, 180];
    for iTr = 1 : exp_opt.len_max_tr
        rng(iTr*7);
        sel_ang = rot_ang(randi(2, 1));
        init_rot_mat = [cosd(sel_ang), -sind(sel_ang); sind(sel_ang), cosd(sel_ang)];
        exp_opt.init_loc(iTr, :) = vis_opt.house_loc + (init_rot_mat*exp_opt.orig_vect')';
    end
    
    % Mouse related experiment configurations.
    exp_opt.sensitive_r = vis_opt.cream_sz(1)+vis_opt.animal_sz(1); % Size of whip cream plus 30 pixel (default = 30 pixel)
    exp_opt.criteria = (exp_opt.sensitive_r*1.15);
    
    % Device opt
    exp_opt.stopkey	= KbName('ESCAPE');
    exp_opt.gokey	= KbName( 'RightArrow' );
    
    exp_opt.rnd_seed = rnd_seed;
    
    %% eye options
    eye_opt.eye_cam_on = false;
    eye_opt.eye_side = 1;
end