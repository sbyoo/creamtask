function [animal_idx, house_idx, sel_target] = determine_combination(rnd_seed, l_tr, n_animal, n_house, animal_obj_map, house_obj_map)

    % The way the combination is determined by randomly picking either of them.
    %   Each index indicates the target each animal or house is directing.
    %   Frequency of the animal/house appearance is roughly matched.
    %   Outcome is random.
    
    % For phase two, there should be a increased combination.    
    [animal_idx, house_idx] = replicate([]);
    l_tot = 0;
        
    while l_tot <= l_tr
        rnd_animal = repmat(1:n_animal, n_house, 1);
        rnd_house  = repmat(1:n_house, n_animal, 1);
        
        rng(rnd_seed,'twister')
        animal_idx = [animal_idx; datasample(rnd_animal(:), n_animal*n_house, 'replace', false)]; %#ok<AGROW>
        rng(rnd_seed+10,'twister')
        house_idx = [house_idx; datasample(rnd_house(:), n_animal*n_house, 'replace', false)]; %#ok<AGROW>
    
        l_tot = length(animal_idx);
    end
    
    if l_tot > l_tr
        animal_idx(l_tot:-1:l_tr+1) =[ ];
        house_idx(l_tot:-1:l_tr+1) =[ ];
    end
    
    sel_target = zeros(l_tr, 1);
    rng(1234,'twister');
    a_or_h = randi( [1, 2], l_tr, 1);
    for iTr = 1 : l_tr
        if a_or_h(iTr) == 1
            sel_target(iTr, 1) = animal_obj_map(animal_idx(iTr), 2);           
        else
            sel_target(iTr, 1) = house_obj_map(house_idx(iTr), 2);           
        end
    end
end