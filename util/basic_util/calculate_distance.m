function dist_out = calculate_distance(x, y)

    %% input is two dimensional (for the purpose of pacman).
    %   In here, x is derivative of position 1 and y is derivative of position 2. 
    dist_out = sqrt(sum(x.^2+y.^2, 2));
end