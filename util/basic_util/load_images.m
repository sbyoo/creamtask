function loaded_img = load_images(im_dir, max_num)

    cd(im_dir);
    im_files = dir('*png*');
    n_files  = length(im_files);
    
    % Make sure that number of images are matching with experimental design.
    assert(n_files >= max_num)
    
    loaded_img = cell(max_num, 1);
    for nF = 1 : max_num 
        [loaded_img{nF}] = imread(im_files(nF).name);
    end    
end