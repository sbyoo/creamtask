function agent_fut_pos = update_position(agent_curr_pos, goal_pos, exp_opt)

    %% This function calculates the slope between positions and updates upcomping position.
    %   Curr position is animal's potision
    %   Goal position is either house or object position.
 
    if agent_curr_pos(1) ~= goal_pos(1) % If the target is on the right side of the house.
        % Determine slope and constant when animal is appearing.
        coeffs = polyfit([agent_curr_pos(1), goal_pos(1)], [agent_curr_pos(2), goal_pos(2)], 1);
        
        if agent_curr_pos(1) > goal_pos(1)
            agent_fut_pos(1) = agent_curr_pos(1)-exp_opt.move_spd(1);
        else
            agent_fut_pos(1) = agent_curr_pos(1)+exp_opt.move_spd(1);
        end
        agent_fut_pos(2) = coeffs(1)*agent_fut_pos(1)+coeffs(2);
    else
        agent_fut_pos(1) = agent_curr_pos(1);
        
        % This is doable because the there is no downward movement.
        if agent_curr_pos(2) <= goal_pos(2)
            agent_fut_pos(2) = agent_curr_pos(2)+exp_opt.move_spd(1);
        else
            agent_fut_pos(2) = agent_curr_pos(2)-exp_opt.move_spd(1);            
        end
    end
end