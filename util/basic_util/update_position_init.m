function agent_fut_pos = update_position_init(agent_curr_pos, goal_pos, exp_opt)

    % Determine slope and constant when animal is appearing.
    coeffs = polyfit([agent_curr_pos(1), goal_pos(1)], [agent_curr_pos(2), goal_pos(2)], 1);
    
    if agent_curr_pos(1) > goal_pos(1)
        agent_fut_pos(1) = agent_curr_pos(1)-exp_opt.move_spd(1);
    else
        agent_fut_pos(1) = agent_curr_pos(1)+exp_opt.move_spd(1);
    end
    agent_fut_pos(2) = coeffs(1)*agent_fut_pos(1)+coeffs(2);
end