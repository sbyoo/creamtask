function [time_info, trial_onset] = check_keypress_inplay(exp_opt, time_info) 
	
    global stage

	%% Manual Keyboard Commands (make into subfunction and fix details)
	KbEventFlush( );
	[ keyPressed, ~, keyCode ] = KbCheck( );
    trial_onset = true;
	
    if (keyPressed)
        if keyCode(exp_opt.stopkey)
            stage = 10;
            trial_onset = false;
            time_info.endTime = GetSecs( );	% Remaining absolute time stamp.
            disp( 'Task stopped!!!' );
        end
    end
end