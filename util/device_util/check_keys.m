function [go_sign, time_info] = check_keys(go_sign, exp_opt)

	KbEventFlush( );   
    
    %% Check whether key is pressed.
    while( go_sign == 0 )
        [ keyPressed, ~, keyCode ] = KbCheck( );
	
        if (keyPressed)
            if keyCode(exp_opt.gokey)
                go_sign = 1;
                time_info.start_time = GetSecs( );	% Remaining absolute time stamp.
                disp('key pressed');
            end
        end
    end
end