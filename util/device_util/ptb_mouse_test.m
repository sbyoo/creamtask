function mouse_test

    vis_opt.bg_col = [50, 50, 50];
    vis_opt.screen = 1;
    [vis_opt.wndw, vis_opt.wdwRect] = Screen('OpenWindow', vis_opt.screen, vis_opt.bg_col);
    
    sess_onset = 0;
    disp( 'Right Arrow to start | Escape to Exit' ); % Asking experimenter to start and exit.
    sess_onset = check_keys( sess_onset );

    while sess_onset
        [x_loc,y_loc,buttons] = GetMouse(vis_opt.wndw);

        disp('-----')
        disp( ['locs: ', num2str(x_loc), ' and ', num2str(y_loc)] );
        disp( ['button press: ', num2str(buttons)] );
        disp('-----')

        if buttons(1)
            sess_onset = false;
            disp('test complete');
        end
    end

end