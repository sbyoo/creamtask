function save_data_whipcream(subjID, paths, exp_opt, vis_opt, trial_idx)
   
	%% PURPOSE OF THIS FUNCTION)
	%	Save data : try to organize well
	%	Adopt catstruct function obtained from matlab file exchange. 
	%	Then concatenate whole structure into single data. 
	    
    % Define file relevant names
    %   Check whether there are files already.
    %   If there are files, make the function to avoid overlap between files. 
    
    cd(paths.save_data); 
    files = dir('*.mat*');
    file_num = length(files);
    
    if trial_idx ~= file_num+1 % Due to iteration system
       disp('The subject has not reached to maximum number of trials.'); 
    end
    
    outcome = cell(file_num, 1);
    for nF = 1 : file_num
        load(files(nF).name)
        outcome{nF}.choice_info = choice_info;
        outcome{nF}.time_info = time_info;
        outcome{nF}.eye_data  = eye_data;
        outcome{nF}.trial_idx = nF;
    end
    
	% whole_data = catstruct( device_opt, game_opt, visual_opt, temporal_opt, color_opt, days, data ); 
	options = catstruct(exp_opt, vis_opt); 
	
	% Path-Def and save files in there.
    save_filename  = [datestr(now, 'mmddyyyy'), '_', subjID, '_whipcream.mat'];
    paths.sum_data = fullfile(paths.save_data, 'overall');
    if ~exist(paths.sum_data, 'dir')
        mkdir(paths.sum_data);
    end
    cd(paths.sum_data);
	save( save_filename, 'outcome', 'options' );
	cd(paths.exc_path);
end