function time_info = save_trial_data(paths, eye_data, choice_info, time_info, trial_idx)

    global stage

    time_info.save_start = GetSecs();

    % Load all data and combined into single file.
    cd(paths.save_data);
    format_out  = 'mmddyyyy';
    exp_date    = datestr(now, format_out);
    file_name   = [exp_date, '_trial_', num2str(trial_idx)];
    save(file_name, 'choice_info', 'time_info', 'eye_data');
    cd(paths.exc_path);
    
    time_info.save_end = GetSecs();
    stage = 0; % Always
end